name 'yetcd'
maintainer 'Tsang Yong'
maintainer_email 'tsang.yong@gmail.com'
license 'all_rights'
description 'Installs/Configures yetcd'
long_description 'Installs/Configures yetcd'
version '0.1.2'

depends 'etcd', '~> 3.0'
