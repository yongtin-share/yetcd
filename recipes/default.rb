#
# Cookbook Name:: yetcd
# Recipe:: default
#
# Copyright (c) 2016 Tsang Yong, All Rights Reserved.


etcd_installation_binary 'default' do
  version '3.1.7'
  checksum 'd39c6ccaec8e1f8f7ee6591f17d95adc'
  action :create
end

etcd_service node['hostname'] do
  advertise_client_urls 'http://127.0.0.1:2379,http://127.0.0.1:4001'
  listen_client_urls 'http://0.0.0.0:2379,http://0.0.0.0:4001'
  initial_advertise_peer_urls "http://#{node['ipaddress']}:2380"
  listen_peer_urls "http://#{node['ipaddress']}:2380"
  initial_cluster_token 'etcd-cluster-1'
  initial_cluster node['yetcd']['service']['initial_cluster'].join(',')
  initial_cluster_state 'new'
  action :start
end
